#!/bin/sh
url="https://www.europarl.europa.eu/meps/en/full-list/xml"

IDS=$(wget -o /dev/null -O - $url | xq -r ".meps | .[] | map(select($1)) | map( .id ) |(.[])")

id2mail () {
  wget "https://www.europarl.europa.eu/meps/en/$1" -o /dev/null -O -|grep link_email|cut -d\" -f4|rev|sed 's/\]ta\[/@/g'|sed 's/\]tod\[/\./g'|grep @|head -n 1
}
export -f id2mail

xargs -a<(cat <<< "$IDS") -d "\n" -I {} -P 16 sh -c "id2mail {}"

# usage
#sh MEP-mail.sh '.country == "France"'
#sh MEP-mail.sh '.fullName == "Stéphanie YON-COURTIN"'
#sh MEP-mail.sh '.politicalGroup == "Renew Europe Group"'
#sh MEP-mail.sh '.nationalPoliticalGroup == "Liste Renaissance"'
#sh MEP-mail.sh 'true'
