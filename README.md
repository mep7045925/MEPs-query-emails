# MEPs-query-emails
A simple shell script to collect MEPs emails.
# Prerequisite: 
- [xq (bundled with yq)](https://github.com/mikefarah/yq)
# Usage:
```shell
sh MEP-mail.sh <xq formated query>
```
## Examples
```shell
# Get email for every MEPs of Poland:
sh MEP-mail.sh '.country == "Poland"'
# Search by name (not practical, you need the exact name):
sh MEP-mail.sh '.fullName == "Magdalena ADAMOWICZ"'
# Search by political group:
sh MEP-mail.sh '.politicalGroup == "Group of the European People'"'"'s Party (Christian Democrats)"'
# Search by national political group:
sh MEP-mail.sh '.nationalPoliticalGroup == "Independent"'
# Get every emails:
sh MEP-mail.sh 'true'
```
PS : The above example search is just the first result and isn't representative of my political interest (I'm not even from Poland).

You can create more complex query using the yq format, one good application of this would be to create fuzzy search, non case-sensitive seach.
WARNING: Make shure to correcly escape your strings : `'.politicalGroup == "Group of the European People's Party (Christian Democrats)"'` -> `'.politicalGroup == "Group of the European People'"'"'s Party (Christian Democrats)"'`
## Tips and trics:
### Look at the json format used by yq: 
```shell
wget -o /dev/null -O - 'https://www.europarl.europa.eu/meps/en/full-list/xml' | xq -r ".meps | .[][0]"
```
output:
```
{
  "fullName": "Magdalena ADAMOWICZ",
  "country": "Poland",
  "politicalGroup": "Group of the European People's Party (Christian Democrats)",
  "id": "197490",
  "nationalPoliticalGroup": "Independent"
}
```
### Parallelism:
By default I use 16 parallel querries to make it faster (`-P 16` in xargs) this value is hardcoded on purpose as I could sometimes get rate limited using 32 and have less emails than expected.
### MEPs with multiple emails
Some MEPs have multiple emails, this script ensure that only 1 email is exactracted per MEP, this behaviour can be changed in the `id2mail` function by removing `head -n 1` 
# Special thanks
- [MEP-email](https://github.com/marado/MEP-emails) base for my script.
- [yq](https://github.com/mikefarah/yq) utility used to parse XML data
